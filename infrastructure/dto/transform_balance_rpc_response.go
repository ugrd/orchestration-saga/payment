package dto

import (
	"gitlab.com/ugrd/orchestration-saga/payment/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// TransformBalanceToRPCResponse is a function to transform
func TransformBalanceToRPCResponse(payload *entity.Balance) *payment.Balance {
	var balanceHistories []*payment.BalanceHistory
	for _, history := range payload.Histories {
		balanceHistories = append(balanceHistories, &payment.BalanceHistory{
			Id:        uint64(history.ID),
			Type:      history.HistoryType,
			Amount:    history.Amount,
			CreatedAt: timestamppb.New(history.CreatedAt),
			UpdatedAt: timestamppb.New(history.UpdatedAt),
		})
	}

	return &payment.Balance{
		Id:            uint64(payload.ID),
		UserId:        uint64(payload.UserID),
		CurrentAmount: payload.CurrentAmount,
		UsedAmount:    payload.UsedAmount,
		CreatedAt:     timestamppb.New(payload.CreatedAt),
		UpdatedAt:     timestamppb.New(payload.UpdatedAt),
		DeletedAt:     timestamppb.New(payload.DeletedAt.Time),
		Histories:     balanceHistories,
	}
}

// TransformListBalanceToRPCResponse is a function to transform
func TransformListBalanceToRPCResponse(payloads []*entity.Balance) *payment.ListBalance {
	var balances []*payment.Balance
	for _, balance := range payloads {
		var balanceHistories []*payment.BalanceHistory

		for _, r := range balance.Histories {
			balanceHistories = append(balanceHistories, &payment.BalanceHistory{
				Id:        uint64(r.ID),
				Type:      r.HistoryType,
				Amount:    r.Amount,
				CreatedAt: timestamppb.New(r.CreatedAt),
				UpdatedAt: timestamppb.New(r.UpdatedAt),
			})
		}

		balances = append(balances, &payment.Balance{
			Id:            uint64(balance.ID),
			UserId:        uint64(balance.UserID),
			CurrentAmount: balance.CurrentAmount,
			UsedAmount:    balance.UsedAmount,
			CreatedAt:     timestamppb.New(balance.CreatedAt),
			UpdatedAt:     timestamppb.New(balance.UpdatedAt),
			DeletedAt:     timestamppb.New(balance.DeletedAt.Time),
			Histories:     balanceHistories,
		})
	}

	return &payment.ListBalance{
		Balances: balances,
	}
}
