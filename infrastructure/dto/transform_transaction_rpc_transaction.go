package dto

import (
	"gitlab.com/ugrd/orchestration-saga/payment/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// TransformTransactionToRPCResponse is a function to transform
func TransformTransactionToRPCResponse(payload *entity.Transaction) *payment.Transaction {
	return &payment.Transaction{
		Id:            uint64(payload.ID),
		UserId:        uint64(payload.UserID),
		TransactionId: payload.TransactionID,
		Amount:        payload.Amount,
		Status:        payload.Status,
		CreatedAt:     timestamppb.New(payload.CreatedAt),
		UpdatedAt:     timestamppb.New(payload.UpdatedAt),
	}
}

// TransformTransactionListToRPCResponse is a function to transform
func TransformTransactionListToRPCResponse(payloads []*entity.Transaction) *payment.ListTransaction {
	var transactions []*payment.Transaction
	for _, transaction := range payloads {
		transactions = append(transactions, TransformTransactionToRPCResponse(transaction))
	}

	return &payment.ListTransaction{
		Transactions: transactions,
	}
}
