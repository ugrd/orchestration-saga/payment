package seeder

import (
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/registry"
)

// Seed is a contract
type Seed interface {
	Seed(repo *registry.Repositories) error
}
