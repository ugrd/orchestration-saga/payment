package core

import (
	"encoding/base64"
	"gitlab.com/ugrd/orchestration-saga/package/security/jwt"
	"gitlab.com/ugrd/orchestration-saga/payment/config"
)

// NewJWT is constructor
func NewJWT(conf *config.Config) (*jwt.JWT, error) {
	privateKey, errPri := base64.StdEncoding.DecodeString(conf.AppPrivateKey)
	publicKey, errPub := base64.StdEncoding.DecodeString(conf.AppPublicKey)

	if errPub != nil {
		return nil, errPri
	}

	if errPub != nil {
		return nil, errPub
	}

	j := jwt.NewJWT(privateKey, publicKey)

	return j, nil
}
