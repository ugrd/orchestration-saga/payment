package persistence

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/payment/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/payment/domain/repository"
	"gitlab.com/ugrd/orchestration-saga/payment/pkg/common"
	"gorm.io/gorm"
)

// HistoryRepo is a struct
type HistoryRepo struct {
	db *gorm.DB
}

// Create is a method to create new role
func (r HistoryRepo) Create(ctx context.Context, history *entity.History) error {
	return r.db.WithContext(ctx).Create(history).Error
}

// Update is a method to update existing role
func (r HistoryRepo) Update(ctx context.Context, history *entity.History, id uint) error {
	return r.db.WithContext(ctx).Where("id = ?", id).Updates(history).Error
}

// Delete is a method to remove existing role
func (r HistoryRepo) Delete(ctx context.Context, id uint) error {
	return r.db.WithContext(ctx).Delete(&entity.History{}, id).Error
}

// FindByID is a method to retrieve data by id
func (r HistoryRepo) FindByID(ctx context.Context, id uint) (*entity.History, error) {
	var history entity.History

	err := r.db.WithContext(ctx).
		Where("id = ?", id).
		Take(&history).Error
	if err != nil {
		return nil, err
	}

	return &history, nil
}

// FindByBalanceID is a method to retrieve data by user id
func (r HistoryRepo) FindByBalanceID(ctx context.Context, balanceID uint) ([]*entity.History, error) {
	var histories []*entity.History

	err := r.db.WithContext(ctx).
		Where("balance_id = ?", balanceID).
		Find(&histories).Error
	if err != nil {
		return nil, err
	}

	return histories, nil
}

// FindByTransactionID is a method to retrieve data by transaction id
func (r HistoryRepo) FindByTransactionID(ctx context.Context, transactionID string) (*entity.History, error) {
	var history entity.History

	err := r.db.WithContext(ctx).
		Where("transaction_id = ?", transactionID).
		Take(&history).Error
	if err != nil {
		return nil, err
	}

	return &history, nil
}

// FindAll is a method to retrieve transports
func (r HistoryRepo) FindAll(ctx context.Context, _ *common.FilterQuery) ([]*entity.History, error) {
	var histories []*entity.History

	err := r.db.WithContext(ctx).
		Find(&histories).Error
	if err != nil {
		return nil, err
	}

	return histories, nil
}

// NewHistoryRepo is a constructor
func NewHistoryRepo(db *gorm.DB) *HistoryRepo {
	return &HistoryRepo{db: db}
}

var _ repository.HistoryRepoInterface = &HistoryRepo{}
