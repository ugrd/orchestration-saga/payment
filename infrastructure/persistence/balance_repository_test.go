package persistence_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/ugrd/orchestration-saga/payment/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/payment/pkg/common"
	"gitlab.com/ugrd/orchestration-saga/payment/pkg/consts"
	"gitlab.com/ugrd/orchestration-saga/payment/tests"
	"gorm.io/gorm"
	"testing"
)

func TestBalanceRepo_FindByUserID(t *testing.T) {
	testbox := tests.Init()
	ctx := testbox.Ctx
	repo := testbox.Repo

	const (
		userID = 1
		ID     = 1
	)

	t.Run("it should be valid save", func(t *testing.T) {
		err := repo.DB.Transaction(func(tx *gorm.DB) error {
			if err := tx.WithContext(ctx).Create(&entity.Balance{
				ID:            ID,
				UserID:        userID,
				CurrentAmount: 10000,
				UsedAmount:    0,
			}).Error; err != nil {
				return err
			}

			if err := tx.WithContext(ctx).Create(&entity.History{
				BalanceID:   ID,
				HistoryType: consts.Debit,
				Amount:      10000,
			}).Error; err != nil {
				return err
			}

			return nil
		})

		assert.NoError(t, err)
	})

	t.Run("it should be valid find by user id", func(t *testing.T) {
		r, err := repo.BalanceRepo.FindByUserID(ctx, userID)

		assert.NoError(t, err)
		assert.NotNil(t, r)

	})
}

func TestHistoryRepo_FindAll(t *testing.T) {
	testbox := tests.Init()
	ctx := testbox.Ctx
	repo := testbox.Repo

	const (
		userID = 1
		ID     = 1
	)

	t.Run("it should be valid save", func(t *testing.T) {
		err := repo.DB.Transaction(func(tx *gorm.DB) error {
			if err := tx.WithContext(ctx).Create(&entity.Balance{
				ID:            ID,
				UserID:        userID,
				CurrentAmount: 10000,
				UsedAmount:    0,
			}).Error; err != nil {
				return err
			}

			if err := tx.WithContext(ctx).Create(&entity.History{
				BalanceID:   ID,
				HistoryType: consts.Debit,
				Amount:      10000,
			}).Error; err != nil {
				return err
			}

			return nil
		})

		assert.NoError(t, err)
	})

	t.Run("it should be valid find all", func(t *testing.T) {
		r, err := repo.BalanceRepo.FindAll(ctx, &common.FilterQuery{})

		assert.NoError(t, err)
		assert.NotEmpty(t, r)
	})
}
