package persistence

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/payment/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/payment/domain/repository"
	"gitlab.com/ugrd/orchestration-saga/payment/pkg/common"
	"gorm.io/gorm"
)

// BalanceRepo is a struct
type BalanceRepo struct {
	db *gorm.DB
}

// Create is a method to create new role
func (r BalanceRepo) Create(ctx context.Context, balance *entity.Balance) error {
	return r.db.WithContext(ctx).Create(balance).Error
}

// Update is a method to update existing role
func (r BalanceRepo) Update(ctx context.Context, balance *entity.Balance, id uint) error {
	return r.db.WithContext(ctx).Where("id = ?", id).Updates(balance).Error
}

// Delete is a method to remove existing role
func (r BalanceRepo) Delete(ctx context.Context, id uint) error {
	return r.db.WithContext(ctx).Delete(&entity.Balance{}, id).Error
}

// FindByID is a method to retrieve data by id
func (r BalanceRepo) FindByID(ctx context.Context, id uint) (*entity.Balance, error) {
	var balance entity.Balance

	err := r.db.WithContext(ctx).
		Preload("Histories").
		Where("balances.id = ?", id).
		First(&balance).Error
	if err != nil {
		return nil, err
	}

	return &balance, nil
}

// FindByUserID is a method to retrieve data by user id
func (r BalanceRepo) FindByUserID(ctx context.Context, userID uint) (*entity.Balance, error) {
	var balance entity.Balance

	err := r.db.WithContext(ctx).
		Preload("Histories").
		Where("balances.user_id = ?", userID).
		First(&balance).Error
	if err != nil {
		return nil, err
	}

	return &balance, nil
}

// FindAll is a method to retrieve transports
func (r BalanceRepo) FindAll(ctx context.Context, _ *common.FilterQuery) ([]*entity.Balance, error) {
	var balances []*entity.Balance

	err := r.db.WithContext(ctx).
		Preload("Histories").
		Find(&balances).Error
	if err != nil {
		return nil, err
	}

	return balances, nil
}

// NewBalanceRepo is a constructor
func NewBalanceRepo(db *gorm.DB) *BalanceRepo {
	return &BalanceRepo{db: db}
}

var _ repository.BalanceRepoInterface = &BalanceRepo{}
