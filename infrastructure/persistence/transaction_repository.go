package persistence

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/payment/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/payment/domain/repository"
	"gitlab.com/ugrd/orchestration-saga/payment/pkg/common"
	"gorm.io/gorm"
)

// TransactionRepo is a struct
type TransactionRepo struct {
	db *gorm.DB
}

// Create is a method to create new role
func (r *TransactionRepo) Create(ctx context.Context, transaction *entity.Transaction) error {
	return r.db.WithContext(ctx).Create(transaction).Error
}

// Update is a method to update existing role
func (r *TransactionRepo) Update(ctx context.Context, transaction *entity.Transaction, id uint) error {
	return r.db.WithContext(ctx).Where("id = ?", id).Updates(transaction).Error
}

// Delete is a method to remove existing role
func (r *TransactionRepo) Delete(ctx context.Context, id uint) error {
	return r.db.WithContext(ctx).Delete(&entity.Transaction{}, id).Error
}

// FindByID is a method to retrieve data by id
func (r *TransactionRepo) FindByID(ctx context.Context, id uint) (*entity.Transaction, error) {
	var transaction entity.Transaction

	err := r.db.WithContext(ctx).
		Where("id = ?", id).
		First(&transaction).Error
	if err != nil {
		return nil, err
	}

	return &transaction, nil
}

// FindByTransactionID is a method to retrieve data by user id
func (r *TransactionRepo) FindByTransactionID(ctx context.Context, transactionID string) (*entity.Transaction, error) {
	var transaction entity.Transaction

	err := r.db.WithContext(ctx).
		Where("transaction_id = ?", transactionID).
		First(&transaction).Error
	if err != nil {
		return nil, err
	}

	return &transaction, nil
}

// FindAll is a method to retrieve transports
func (r *TransactionRepo) FindAll(ctx context.Context, _ *common.FilterQuery) ([]*entity.Transaction, error) {
	var transactions []*entity.Transaction

	err := r.db.WithContext(ctx).
		Find(&transactions).Error
	if err != nil {
		return nil, err
	}

	return transactions, nil
}

// NewTransactionRepo is a constructor
func NewTransactionRepo(db *gorm.DB) *TransactionRepo {
	return &TransactionRepo{db: db}
}

var _ repository.TransactionRepoInterface = &TransactionRepo{}
