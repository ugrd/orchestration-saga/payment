package registry

import (
	"gitlab.com/ugrd/orchestration-saga/payment/domain/repository"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/persistence"
	"gorm.io/gorm"
)

// Repositories is a struct which collect repositories
type Repositories struct {
	DB              *gorm.DB
	BalanceRepo     repository.BalanceRepoInterface
	HistoryRepo     repository.HistoryRepoInterface
	TransactionRepo repository.TransactionRepoInterface
}

// NewRepo is constructor
func NewRepo(db *gorm.DB) *Repositories {
	return &Repositories{
		DB:              db,
		BalanceRepo:     persistence.NewBalanceRepo(db),
		HistoryRepo:     persistence.NewHistoryRepo(db),
		TransactionRepo: persistence.NewTransactionRepo(db),
	}
}
