package registry

import (
	"gitlab.com/ugrd/orchestration-saga/payment/domain/contract"
	"gitlab.com/ugrd/orchestration-saga/payment/domain/entity"
)

// CollectEntities is function collects entities
func CollectEntities() []contract.Entity {
	return []contract.Entity{
		{Entity: entity.Balance{}},
		{Entity: entity.History{}},
		{Entity: entity.Transaction{}},
	}
}

// CollectTables is function collects entity names
func CollectTables() []contract.Table {
	var balance entity.Balance
	var history entity.History
	var transaction entity.Transaction

	return []contract.Table{
		{Name: balance.TableName()},
		{Name: history.TableName()},
		{Name: transaction.TableName()},
	}
}
