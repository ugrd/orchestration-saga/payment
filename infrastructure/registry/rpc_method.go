package registry

import "gitlab.com/ugrd/orchestration-saga/package/grpc/auth"

// MethodOwner is a method
type MethodOwner struct {
	Owner     []string `json:"owner"`
	Protected bool     `json:"protected"`
}

// RPCMethods is a function to hold grpc service methods
// false value indicates that the method is not protected (no authorization needed)
func RPCMethods() auth.MethodTypes {
	return map[string]*auth.MethodOwner{
		"/master.account.AgentService/GetRoleByID":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.AgentService/GetListRole":  {Owners: []string{"USER", "ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.AgentService/CreateAgent":  {Owners: []string{"SUPER_ADMIN"}, Protect: true},
		"/master.account.AgentService/UpdateAgent":  {Owners: []string{"SUPER_ADMIN"}, Protect: true},
		"/master.account.AgentService/DeleteAgent":  {Owners: []string{"SUPER_ADMIN"}, Protect: true},
		"/master.account.AgentService/GetAgentByID": {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.AgentService/GetListAgent": {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.UserService/CreateUser":    {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.UserService/UpdateUser":    {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.UserService/DeleteUser":    {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.UserService/FindUser":      {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.UserService/GetUser":       {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
	}
}
