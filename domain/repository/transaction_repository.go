package repository

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/payment/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/payment/pkg/common"
)

// TransactionRepoInterface is transaction repo contract
type TransactionRepoInterface interface {
	Create(ctx context.Context, transaction *entity.Transaction) error
	Update(ctx context.Context, transaction *entity.Transaction, id uint) error
	Delete(ctx context.Context, id uint) error
	FindByID(ctx context.Context, id uint) (*entity.Transaction, error)
	FindByTransactionID(ctx context.Context, transactionID string) (*entity.Transaction, error)
	FindAll(ctx context.Context, filter *common.FilterQuery) ([]*entity.Transaction, error)
}
