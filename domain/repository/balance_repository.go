package repository

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/payment/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/payment/pkg/common"
)

// BalanceRepoInterface is balance repo contract
type BalanceRepoInterface interface {
	Create(ctx context.Context, balance *entity.Balance) error
	Update(ctx context.Context, balance *entity.Balance, id uint) error
	Delete(ctx context.Context, id uint) error
	FindByID(ctx context.Context, id uint) (*entity.Balance, error)
	FindByUserID(ctx context.Context, userID uint) (*entity.Balance, error)
	FindAll(ctx context.Context, filter *common.FilterQuery) ([]*entity.Balance, error)
}
