package repository

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/payment/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/payment/pkg/common"
)

// HistoryRepoInterface is history repo contract
type HistoryRepoInterface interface {
	Create(ctx context.Context, balance *entity.History) error
	Update(ctx context.Context, balance *entity.History, id uint) error
	Delete(ctx context.Context, id uint) error
	FindByID(ctx context.Context, id uint) (*entity.History, error)
	FindByBalanceID(ctx context.Context, balanceID uint) ([]*entity.History, error)
	FindByTransactionID(ctx context.Context, transactionID string) (*entity.History, error)
	FindAll(ctx context.Context, filter *common.FilterQuery) ([]*entity.History, error)
}
