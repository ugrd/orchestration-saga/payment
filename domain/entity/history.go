package entity

import (
	"gitlab.com/ugrd/orchestration-saga/payment/domain/contract"
	"time"
)

// History is a struct that represents histories table
type History struct {
	ID            uint      `gorm:"column:id;not null;uniqueIndex;primaryKey" json:"id"`
	BalanceID     uint      `gorm:"column:balance_id;not null;index" json:"balance_id"`
	TransactionID string    `gorm:"column:transaction_id;not null;uniqueIndex" json:"transaction_id"`
	HistoryType   string    `gorm:"column:history_type;not null" json:"type"`
	Amount        uint64    `gorm:"column:amount;not null" json:"amount"`
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
	Balance       Balance   `json:"balance"`
}

// Implements base entity methods
var _ contract.EntityInterface = &History{}

// TableName is a function return table name
func (u *History) TableName() string {
	return "histories"
}

// FilterableFields is a function return filterable fields
func (u *History) FilterableFields() []interface{} {
	return []interface{}{"id", "balance_id", "history_type"}
}

// TimeFields is a function return time fields
func (u *History) TimeFields() []interface{} {
	return []interface{}{"created_at", "updated_at"}
}
