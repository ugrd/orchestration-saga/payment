package entity

// Interface is a set of functions that should be implemented by entity
type Interface interface {
	TableName() string
	FilterableFields() []interface{}
	TimeFields() []interface{}
}
