package entity

import (
	"gitlab.com/ugrd/orchestration-saga/payment/domain/contract"
	"gorm.io/gorm"
	"time"
)

// Balance is a struct that represents balances table
type Balance struct {
	ID            uint           `gorm:"column:id;not null;uniqueIndex;primaryKey" json:"id"`
	UserID        uint           `gorm:"column:user_id;not null;index" json:"user_id"`
	CurrentAmount uint64         `gorm:"column:current_amount;not null" json:"current_amount"`
	UsedAmount    uint64         `gorm:"column:used_amount;not null" json:"used_amount"`
	CreatedAt     time.Time      `json:"created_at"`
	UpdatedAt     time.Time      `json:"updated_at"`
	DeletedAt     gorm.DeletedAt `json:"deleted_at"`
	Histories     []History      `gorm:"foreignKey:BalanceID" json:"histories"`
}

// Implements base entity methods
var _ contract.EntityInterface = &Balance{}

// TableName is a function return table name
func (u *Balance) TableName() string {
	return "balances"
}

// FilterableFields is a function return filterable fields
func (u *Balance) FilterableFields() []interface{} {
	return []interface{}{"id", "user_id"}
}

// TimeFields is a function return time fields
func (u *Balance) TimeFields() []interface{} {
	return []interface{}{"created_at", "updated_at", "deleted_at"}
}
