package entity

import (
	"gitlab.com/ugrd/orchestration-saga/payment/domain/contract"
	"gorm.io/gorm"
	"time"
)

// Transaction is a struct that represents transactions table
type Transaction struct {
	ID            uint           `gorm:"column:id;not null;uniqueIndex;primaryKey" json:"id"`
	UserID        uint           `gorm:"column:user_id;not null;index" json:"user_id"`
	TransactionID string         `gorm:"column:transaction_id;not null;uniqueIndex" json:"transaction_id"`
	Amount        uint64         `gorm:"column:amount;not null" json:"amount"`
	Status        string         `gorm:"column:status;not null" json:"status"`
	Description   string         `gorm:"description" json:"description"`
	CreatedAt     time.Time      `json:"created_at"`
	UpdatedAt     time.Time      `json:"updated_at"`
	DeletedAt     gorm.DeletedAt `json:"deleted_at"`
}

// Status is a type
type Status string

const (
	// Pending is a constant value
	Pending = Status("PENDING")
	// Failed is a constant value
	Failed = Status("FAILED")
	// Completed is a constant value
	Completed = Status("COMPLETED")
)

// Implements base entity methods
var _ contract.EntityInterface = &Transaction{}

// TableName is a function return table name
func (u *Transaction) TableName() string {
	return "transactions"
}

// FilterableFields is a function return filterable fields
func (u *Transaction) FilterableFields() []interface{} {
	return []interface{}{"id", "user_id", "transaction_id"}
}

// TimeFields is a function return time fields
func (u *Transaction) TimeFields() []interface{} {
	return []interface{}{"created_at", "updated_at", "deleted_at"}
}
