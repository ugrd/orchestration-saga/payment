package consts

const (
	// Debit is a constant value
	Debit = "DEBIT"
	// Credit is a constant value
	Credit = "CREDIT"
)

// TypeList is constant type list
func TypeList() []string {
	return []string{
		Debit,
		Credit,
	}
}
