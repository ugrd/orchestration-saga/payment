package service

import (
	"gitlab.com/ugrd/orchestration-saga/package/security/jwt"
	"gitlab.com/ugrd/orchestration-saga/payment/config"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/registry"
)

// Option is a function
type Option func(*GRPCService)

// WithConfig is a function option
func WithConfig(conf *config.Config) Option {
	return func(service *GRPCService) {
		service.config = conf
	}
}

// WithRepo is a function option
func WithRepo(repo *registry.Repositories) Option {
	return func(service *GRPCService) {
		service.repo = repo
	}
}

// WithJWT is a function option
func WithJWT(j *jwt.JWT) Option {
	return func(service *GRPCService) {
		service.jwt = j
	}
}
