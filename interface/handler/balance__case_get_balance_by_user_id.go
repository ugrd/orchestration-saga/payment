package handler

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/dto"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"
	"google.golang.org/grpc/codes"
	"gorm.io/gorm"
	"reflect"
)

// GetBalanceByUserID is a method
func (hdl *Handler) GetBalanceByUserID(ctx context.Context, req *payment.BalanceIdentifierByUserIDRequest) (*payment.Balance, error) {
	userId := req.GetUserId()
	if reflect.ValueOf(userId).IsZero() {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(map[string]interface{}{
				"user_id": "is required",
			}).ErrList()
	}

	r, err := hdl.Dependency.Repository.BalanceRepo.FindByUserID(ctx, uint(userId))
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	return dto.TransformBalanceToRPCResponse(r), nil
}
