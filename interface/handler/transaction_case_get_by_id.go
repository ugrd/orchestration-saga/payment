package handler

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/dto"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"
	"google.golang.org/grpc/codes"
	"gorm.io/gorm"
	"reflect"
)

// GetByID is a method
func (hdl *Handler) GetTransactionByID(ctx context.Context, req *payment.TransactionIdentifierRequest) (*payment.Transaction, error) {
	id := req.GetId()
	if reflect.ValueOf(id).IsZero() {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(map[string]interface{}{
				"id": "is required",
			}).ErrList()
	}

	r, err := hdl.Dependency.Repository.TransactionRepo.FindByID(ctx, uint(id))
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	return dto.TransformTransactionToRPCResponse(r), nil
}
