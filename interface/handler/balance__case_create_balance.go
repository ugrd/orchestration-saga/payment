package handler

import (
	"context"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"gitlab.com/ugrd/orchestration-saga/payment/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/dto"
	"gitlab.com/ugrd/orchestration-saga/payment/pkg/consts"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"
	"google.golang.org/grpc/codes"
	"gorm.io/gorm"
)

// CreateBalance is a method
func (hdl *Handler) CreateBalance(ctx context.Context, req *payment.BalancePaymentRequest) (*payment.Balance, error) {
	payload := &CreateBalancePayload{
		UserID:        req.GetUserId(),
		Amount:        req.GetAmount(),
		TransactionID: req.GetTransactionId(),
	}

	if err := payload.Validate(); err != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(utils.ErrToMap(err)).
			ErrList()
	}

	userID := uint(req.GetUserId())
	// initialize repo
	repo := hdl.Dependency.Repository

	if _, err := repo.HistoryRepo.FindByTransactionID(ctx, req.GetTransactionId()); err == nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(map[string]interface{}{
				"transaction_id": "already taken",
			}).ErrList()
	}

	if resp, err := repo.BalanceRepo.FindByUserID(ctx, userID); err == nil {
		return dto.TransformBalanceToRPCResponse(resp), nil
	}

	// handle transaction
	err := repo.DB.Transaction(func(tx *gorm.DB) error {
		balance := &entity.Balance{
			UserID:        userID,
			CurrentAmount: req.GetAmount(),
			UsedAmount:    0,
		}
		if err := tx.WithContext(ctx).Create(balance).Error; err != nil {
			return err
		}

		if err := tx.WithContext(ctx).
			Create(&entity.History{
				BalanceID:     balance.ID,
				HistoryType:   consts.Debit,
				Amount:        req.GetAmount(),
				TransactionID: req.GetTransactionId(),
			}).Error; err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	resp, _ := repo.BalanceRepo.FindByUserID(ctx, userID)

	return dto.TransformBalanceToRPCResponse(resp), nil
}

// CreateBalancePayload is a type
type CreateBalancePayload struct {
	TransactionID string
	UserID        uint64
	Amount        uint64
}

// Validate is a method
func (c CreateBalancePayload) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.TransactionID, validation.Required, validation.Length(8, 50)),
		validation.Field(&c.UserID, validation.Required),
	)
}
