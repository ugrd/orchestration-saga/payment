package handler

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/dto"
	"gitlab.com/ugrd/orchestration-saga/payment/pkg/common"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"
	"google.golang.org/grpc/codes"
)

// GetListBalance is a method
func (hdl *Handler) GetListBalance(ctx context.Context, query *payment.BalanceFilterQuery) (*payment.ListBalance, error) {
	rows, err := hdl.Dependency.Repository.BalanceRepo.FindAll(ctx, &common.FilterQuery{})
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}
	return dto.TransformListBalanceToRPCResponse(rows), nil
}
