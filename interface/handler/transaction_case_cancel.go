package handler

import (
	"context"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	log "gitlab.com/ugrd/orchestration-saga/package/logger"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"gitlab.com/ugrd/orchestration-saga/payment/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"
)

// CancelTransaction is a method
func (hdl *Handler) CancelTransaction(ctx context.Context, req *payment.CancelTransactionRequest) (*payment.Transaction, error) {
	logger := log.New()

	logger.Info("[CancelTransaction] starting....")

	payload := &CancelTransactionRequest{
		TransactionID: req.GetTransactionId(),
		UserID:        req.GetUserId(),
		Amount:        req.GetAmount(),
	}

	logger.Infof("[CancelTransaction] args: TransactionID: %v, UserID: %v, Amount: %v",
		req.GetTransactionId(),
		req.GetUserId(),
		req.GetAmount(),
	)

	if err := payload.Validate(); err != nil {
		logger.Warnf("[CancelTransaction] error validation: %v", err)
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(utils.ErrToMap(err)).
			ErrList()
	}

	userID := uint(req.GetUserId())
	amount := req.GetAmount()
	transactionID := req.GetTransactionId()

	tr, err := hdl.Dependency.Repository.TransactionRepo.FindByTransactionID(ctx, transactionID)
	if err != nil {
		logger.Warnf("[CancelTransaction] error find transaction err: %v", err)
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	_ = hdl.Dependency.Repository.TransactionRepo.Update(ctx, &entity.Transaction{
		Status: string(entity.Failed),
	}, tr.ID)

	balance, err := hdl.Dependency.Repository.BalanceRepo.FindByUserID(ctx, userID)
	if err != nil {
		logger.Warnf("[CancelTransaction] error finding balance userID: %v, err: %v", userID, err)
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	amount = balance.CurrentAmount + amount
	err = hdl.Dependency.Repository.BalanceRepo.Update(ctx, &entity.Balance{
		CurrentAmount: amount,
	}, balance.ID)
	if err != nil {
		logger.Warnf("[CancelTransaction] error update balance err: %v", err)
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	r, _ := hdl.Dependency.Repository.TransactionRepo.FindByID(ctx, tr.ID)

	logger.Infof("[CancelTransaction] current balance %d", amount)

	return &payment.Transaction{
		Id:            uint64(r.ID),
		TransactionId: r.TransactionID,
		UserId:        uint64(r.UserID),
		Status:        r.Status,
		CreatedAt:     timestamppb.New(r.CreatedAt),
		UpdatedAt:     timestamppb.New(r.UpdatedAt),
	}, nil
}

// CancelTransactionRequest is a struct
type CancelTransactionRequest struct {
	TransactionID string `json:"transaction_id"`
	UserID        uint64 `json:"user_id"`
	Amount        uint64 `json:"amount"`
}

// Validate is a method
func (c CancelTransactionRequest) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.TransactionID, validation.Required, validation.Length(8, 50)),
		validation.Field(&c.UserID, validation.Required),
		validation.Field(&c.Amount, validation.Required),
	)
}
