package handler

import (
	"context"
	"database/sql"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	log "gitlab.com/ugrd/orchestration-saga/package/logger"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"gitlab.com/ugrd/orchestration-saga/payment/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/registry"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// CreateTransaction is a method
func (hdl *Handler) CreateTransaction(ctx context.Context, req *payment.CreateTransactionRequest) (*payment.Transaction, error) {
	logger := log.New()

	logger.Info("[CreateTransaction] starting....")

	payload := &CreateTransactionRequest{
		TransactionID: req.GetTransactionId(),
		UserID:        req.GetUserId(),
		Amount:        req.GetAmount(),
	}
	logger.Infof("[CreateTransaction] args: TransactionID: %v, UserID: %v, Amount: %v",
		req.GetTransactionId(),
		req.GetUserId(),
		req.GetAmount(),
	)
	if err := payload.Validate(); err != nil {
		logger.Warnf("[CreateTransaction] error validation: %v", err)
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(utils.ErrToMap(err)).
			ErrList()
	}

	userID := uint(req.GetUserId())
	amount := req.GetAmount()
	transactionID := req.GetTransactionId()

	db := hdl.Dependency.Repository.DB
	tx := db.Begin(&sql.TxOptions{
		Isolation: sql.LevelRepeatableRead,
	})

	repo := registry.NewRepo(tx)

	_, err := hdl.Dependency.Repository.TransactionRepo.FindByTransactionID(ctx, transactionID)
	if err == nil {
		logger.Info("[CreateTransaction] transaction_id already exist")

		tx.Rollback()
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(map[string]interface{}{
				"transaction_id": "already taken",
			}).ErrList()
	}

	balance, err := repo.BalanceRepo.FindByUserID(ctx, userID)
	if err != nil {
		logger.Warnf("[CreateTransaction] error finding balance userID: %v, err: %v", userID, err)

		tx.Rollback()
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(map[string]interface{}{
				"user_id": "invalid user_id",
			}).ErrList()
	}

	if balance.CurrentAmount < amount {
		tx.Rollback()
		logger.Warnf("[CreateTransaction] insufficient balance: %v, amount: %v", balance.CurrentAmount, amount)
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(map[string]interface{}{
				"amount": "insufficient balance",
			}).ErrList()
	}

	currentBalance := balance.CurrentAmount - amount

	_ = repo.BalanceRepo.Update(ctx, &entity.Balance{
		CurrentAmount: currentBalance,
	}, balance.ID)

	_ = tx.Commit().Error

	transaction := entity.Transaction{
		UserID:        userID,
		TransactionID: transactionID,
		Amount:        amount,
		Status:        string(entity.Completed),
		Description:   fmt.Sprintf("#transaction[%s]", transactionID),
	}

	err = hdl.Repository.TransactionRepo.Create(ctx, &transaction)
	if err != nil {
		logger.Warnf("[CreateTransaction] error create transaction err: %v", err)

		tx.Rollback()
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	r, _ := hdl.Dependency.Repository.TransactionRepo.FindByTransactionID(ctx, transactionID)

	logger.Infof("[CreateTransaction] current balance %d", currentBalance)

	return &payment.Transaction{
		Id:            uint64(r.ID),
		TransactionId: r.TransactionID,
		UserId:        uint64(r.UserID),
		Status:        r.Status,
		CreatedAt:     timestamppb.New(r.CreatedAt),
		UpdatedAt:     timestamppb.New(r.UpdatedAt),
	}, nil
}

// CreateTransactionRequest is a struct
type CreateTransactionRequest struct {
	TransactionID string `json:"transaction_id"`
	UserID        uint64 `json:"user_id"`
	Amount        uint64 `json:"amount"`
}

// Validate is a method
func (c CreateTransactionRequest) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.TransactionID, validation.Required, validation.Length(8, 50)),
		validation.Field(&c.UserID, validation.Required),
		validation.Field(&c.Amount, validation.Required),
	)
}
