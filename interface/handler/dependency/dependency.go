package dependency

import (
	"gitlab.com/ugrd/orchestration-saga/package/security/jwt"
	"gitlab.com/ugrd/orchestration-saga/payment/config"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/registry"
)

// Dependency collects dependencies needed by handler
type Dependency struct {
	Config     *config.Config
	Repository *registry.Repositories
	JWT        *jwt.JWT
}
