package core

import "gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"

// GRPCService collects grpc service server
type GRPCService struct {
	payment.UnimplementedBalanceServiceServer
	payment.UnimplementedTransactionServiceServer
}
