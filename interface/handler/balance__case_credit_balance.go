package handler

import (
	"context"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"gitlab.com/ugrd/orchestration-saga/payment/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/dto"
	"gitlab.com/ugrd/orchestration-saga/payment/pkg/consts"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"
	"google.golang.org/grpc/codes"
	"gorm.io/gorm"
)

// CreditBalance is a method
func (hdl *Handler) CreditBalance(ctx context.Context, req *payment.BalanceMutationRequest) (*payment.Balance, error) {
	payload := &CreditBalancePayload{
		ID:            req.GetId(),
		Amount:        req.GetAmount(),
		TransactionID: req.GetTransactionId(),
	}
	if err := payload.Validate(); err != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(utils.ErrToMap(err)).
			ErrList()
	}

	id := uint(req.GetId())
	amount := req.GetAmount()
	transactionID := req.GetTransactionId()

	// initialize repo
	repo := hdl.Dependency.Repository

	resp, err := repo.BalanceRepo.FindByID(ctx, id)
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	if _, er := repo.HistoryRepo.FindByTransactionID(ctx, transactionID); er == nil {
		return dto.TransformBalanceToRPCResponse(resp), nil
	}

	if resp.CurrentAmount < amount {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.InvalidArgument).
			WithMsg(fmt.Sprintf("Not enough balance.")).
			Err()
	}

	current := resp.CurrentAmount - amount
	used := resp.UsedAmount + amount

	// handle transaction
	err = repo.DB.Transaction(func(tx *gorm.DB) error {
		if er := tx.WithContext(ctx).Updates(&entity.Balance{
			CurrentAmount: current,
			UsedAmount:    used,
		}).Error; err != nil {
			return er
		}

		if er := tx.WithContext(ctx).
			Create(&entity.History{
				BalanceID:     id,
				HistoryType:   consts.Credit,
				Amount:        req.GetAmount(),
				TransactionID: req.GetTransactionId(),
			}).Error; er != nil {
			return er
		}

		return nil
	})
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	resp, _ = repo.BalanceRepo.FindByID(ctx, id)

	return dto.TransformBalanceToRPCResponse(resp), nil
}

// CreditBalancePayload is a type
type CreditBalancePayload struct {
	TransactionID string
	ID            uint64
	Amount        uint64
}

// Validate is a method
func (c CreditBalancePayload) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.TransactionID, validation.Required, validation.Length(8, 50)),
		validation.Field(&c.ID, validation.Required),
		validation.Field(&c.Amount, validation.Required),
	)
}
