package interceptor

import (
	"gitlab.com/ugrd/orchestration-saga/package/security/jwt"
	"gitlab.com/ugrd/orchestration-saga/payment/config"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/registry"
)

// Interceptor is a struct
type Interceptor struct {
	config *config.Config
	repo   *registry.Repositories
	jwt    *jwt.JWT
}

// New is a constructor
func New(conf *config.Config, repo *registry.Repositories, jwt *jwt.JWT) *Interceptor {
	return &Interceptor{
		config: conf,
		repo:   repo,
		jwt:    jwt,
	}
}
