package cmd

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/ugrd/orchestration-saga/package/security"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/core/provider/connection"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/registry"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/seeder"
)

// newMakeSecret is a method command cli to create public & private key
func (cmd *Command) newMakeSecret() *cli.Command {
	return &cli.Command{
		Name:  "create:secret",
		Usage: "A command to create secret key and public key",
		Action: func(c *cli.Context) error {
			secret, err := security.GenerateSecretKey()

			if err != nil {
				return err
			}

			fmt.Println("APP_PRIVATE_KEY:")
			fmt.Println(secret.PrivateKey)
			fmt.Println("APP_PUBLIC_KEY:")
			fmt.Println(secret.PublicKey)

			return nil
		},
	}
}

// newDBMigrate is a method command cli to run db migration
func (cmd *Command) newDBMigrate() *cli.Command {
	return &cli.Command{
		Name:  "db:migrate",
		Usage: "A command to run database migration",
		Action: func(c *cli.Context) error {
			db, errConn := connection.NewDBConnection(cmd.conf)
			if errConn != nil {
				return fmt.Errorf("unable to connect to database: %w", errConn)
			}

			reg := registry.NewRegistry()

			err := reg.AutoMigrate(db)
			if err != nil {
				return fmt.Errorf("cannot run auto migrate: %w", err)
			}

			return nil
		},
	}
}

// newDBSeed is a method command cli to run db seeder
func (cmd *Command) newDBSeed() *cli.Command {
	return &cli.Command{
		Name:  "db:seed",
		Usage: "A command to run database seeder",
		Action: func(c *cli.Context) error {
			seed := seeder.NewSeeder(cmd.repo)
			err := seed.Seed()
			if err != nil {
				return fmt.Errorf("cannot run db seeder: %w", err)
			}

			return nil
		},
	}
}
