package main

import (
	"github.com/joho/godotenv"
	"github.com/urfave/cli/v2"
	"gitlab.com/ugrd/orchestration-saga/package/configurator"
	"gitlab.com/ugrd/orchestration-saga/payment/config"
	"gitlab.com/ugrd/orchestration-saga/payment/grpc/service"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/core"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/core/provider/connection"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/registry"
	"gitlab.com/ugrd/orchestration-saga/payment/interface/cmd"
	"log"
	"os"
)

func main() {
	if errEnv := godotenv.Load(); errEnv != nil {
		log.Fatal("Error loading .env file")
	}

	conf := config.New()

	db, errConn := connection.NewDBConnection(conf)
	if errConn != nil {
		log.Fatalf("unable connect to database, %v", errConn)
	}

	jwt, errJWT := core.NewJWT(conf)
	if errJWT != nil {
		log.Fatalf("unable to initialize JWT, err: %v", errJWT)
	}

	repo := registry.NewRepo(db)

	command := cmd.NewCommand(
		cmd.WithConfig(conf),
		cmd.WithRepo(repo),
	)

	cfg := configurator.New()

	app := cmd.NewCLI()
	app.Commands = command.Build()

	app.Action = func(ctx *cli.Context) error {
		serv := service.NewGRPCService(
			service.WithConfig(conf),
			service.WithRepo(repo),
			service.WithJWT(jwt),
		)

		err := serv.Run(cfg.Payment.RPCPort)
		if err != nil {
			return err
		}

		return nil
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatalf("Unable to run CLI command, err: %v", err)
	}
}
