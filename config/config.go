package config

import (
	"os"
	"strconv"
)

// DBConfig is a struct which hold db config fields
type DBConfig struct {
	DBDriver   string
	DBHost     string
	DBPort     string
	DBUser     string
	DBPassword string
	DBName     string
	DBTimeZone string
	DBLog      bool
}

// DBTestConfig is a struct which hold db test config fields
type DBTestConfig struct {
	DBDriver   string
	DBHost     string
	DBPort     string
	DBUser     string
	DBPassword string
	DBName     string
	DBTimeZone string
	DBLog      bool
}

// KeyConfig is a struct which hold public & secret key
type KeyConfig struct {
	AppPrivateKey string
	AppPublicKey  string
}

// RedisConfig is a struct which hold redis config fields
type RedisConfig struct {
	RedisHost     string
	RedisPort     string
	RedisPassword string
	RedisDB       int
}

// RedisTestConfig is a struct which hold redis test config fields
type RedisTestConfig struct {
	RedisHost     string
	RedisPort     string
	RedisPassword string
	RedisDB       int
}

// Config is struct which hold config fields
type Config struct {
	AppName     string
	AppPort     int
	AppEnv      string
	AppLang     string
	AppTimeZone string
	GRPCPort    int
	KeyConfig
	DBConfig
	DBTestConfig
	RedisConfig
	RedisTestConfig
	DebugMode bool
	TestMode  bool
}

// New is a constructor of config
func New() *Config {
	return &Config{
		AppName:     getEnv("APP_NAME", ""),
		AppEnv:      getEnv("APP_ENV", "development"),
		AppPort:     getEnvAsInt("APP_PORT", 8080),
		AppLang:     getEnv("APP_LANG", "en"),
		AppTimeZone: getEnv("APP_TIMEZONE", "Asia/Jakarta"),
		GRPCPort:    getEnvAsInt("GRPC_PORT", 9002),
		KeyConfig: KeyConfig{
			AppPrivateKey: getEnv("APP_PRIVATE_KEY", "default-private-key"),
			AppPublicKey:  getEnv("APP_PUBLIC_KEY", "default-public-key"),
		},
		DBConfig: DBConfig{
			DBDriver:   getEnv("DB_DRIVER", "postgres"),
			DBHost:     getEnv("DB_HOST", "127.0.0.1"),
			DBPort:     getEnv("DB_PORT", "5432"),
			DBUser:     getEnv("DB_USER", "postgres"),
			DBPassword: getEnv("DB_PASSWORD", ""),
			DBName:     getEnv("DB_NAME", "postgres"),
			DBTimeZone: getEnv("APP_TIMEZONE", "Asia/Jakarta"),
			DBLog:      getEnvAsBool("DB_LOG", false),
		},
		DBTestConfig: DBTestConfig{
			DBDriver:   getEnv("DB_TEST_DRIVER", "postgres"),
			DBHost:     getEnv("DB_TEST_HOST", "127.0.0.1"),
			DBPort:     getEnv("DB_TEST_PORT", "5432"),
			DBUser:     getEnv("DB_TEST_USER", "postgres"),
			DBPassword: getEnv("DB_TEST_PASSWORD", ""),
			DBName:     getEnv("DB_TEST_NAME", "postgres_test"),
			DBTimeZone: getEnv("APP_TIMEZONE", "Asia/Jakarta"),
			DBLog:      getEnvAsBool("DB_TEST_LOG", false),
		},
		RedisConfig: RedisConfig{
			RedisHost:     getEnv("REDIS_HOST", "localhost"),
			RedisPort:     getEnv("REDIS_PORT", "6379"),
			RedisPassword: getEnv("REDIS_PASSWORD", ""),
			RedisDB:       getEnvAsInt("REDIS_DB", 0),
		},
		RedisTestConfig: RedisTestConfig{
			RedisHost:     getEnv("TEST_REDIS_HOST", "localhost"),
			RedisPort:     getEnv("TEST_REDIS_HOST", "6379"),
			RedisPassword: getEnv("TEST_REDIS_HOST", ""),
			RedisDB:       getEnvAsInt("TEST_REDIS_HOST", 0),
		},
		DebugMode: getEnv("APP_ENV", "local") != "production",
		TestMode:  getEnvAsBool("TEST_MODE", false),
	}
}

// getEnv is function to env value as string
func getEnv(key string, defaultVal string) string {
	if value, exist := os.LookupEnv(key); exist {
		return value
	}

	if nextValue := os.Getenv(key); nextValue != "" {
		return nextValue
	}

	return defaultVal
}

// getEnvAsInt is function to get env value as int
func getEnvAsInt(name string, defaultVal int) int {
	valueStr := getEnv(name, "")
	if value, err := strconv.Atoi(valueStr); err == nil {
		return value
	}

	return defaultVal
}

// getEnvAsBool is a function to get env value as boolean
func getEnvAsBool(name string, defaultVal bool) bool {
	valueStr := getEnv(name, "")
	if value, err := strconv.ParseBool(valueStr); err == nil {
		return value
	}

	return defaultVal
}
