package config_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/ugrd/orchestration-saga/payment/config"
	"reflect"
	"testing"
)

func TestDBConfig(t *testing.T) {
	metaDBFields := []string{
		"DBDriver",
		"DBHost",
		"DBPort",
		"DBUser",
		"DBPassword",
		"DBName",
		"DBTimeZone",
		"DBLog",
	}

	t.Run("if valid DBConfig", func(t *testing.T) {
		metaValue := reflect.ValueOf(new(config.DBConfig)).Elem()

		for _, field := range metaDBFields {
			assert.False(t, metaValue.FieldByName(field) == (reflect.Value{}))
		}
	})

	t.Run("if valid DBTestConfig", func(t *testing.T) {
		metaValue := reflect.ValueOf(new(config.DBTestConfig)).Elem()

		for _, field := range metaDBFields {
			assert.False(t, metaValue.FieldByName(field) == (reflect.Value{}))
		}
	})
}
