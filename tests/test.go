package tests

import (
	"context"
	"flag"
	"fmt"
	"github.com/joho/godotenv"
	"gitlab.com/ugrd/orchestration-saga/payment/config"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/core/provider/connection"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/registry"
	"gitlab.com/ugrd/orchestration-saga/payment/infrastructure/seeder"
	"gitlab.com/ugrd/orchestration-saga/payment/interface/handler"
	"gitlab.com/ugrd/orchestration-saga/payment/tests/database"
	"gitlab.com/ugrd/orchestration-saga/payment/util"
	"log"
	"os"
)

// TestSuite is a struct represents its self
type TestSuite struct {
	Config  *config.Config
	Repo    *registry.Repositories
	Handler *handler.Handler
	Ctx     context.Context
}

// Init is a constructor of test suite
func Init() *TestSuite {
	errT := os.Setenv("TEST_MODE", "true")
	if errT != nil {
		log.Fatalf("unable to set test mode, err: %s", errT.Error())
	}

	if err := godotenv.Load(fmt.Sprintf("%s/.env", util.RootDir())); err != nil {
		log.Fatalf("no .env file provided.")
	}

	conf := config.New()

	dbConn, errDBConn := connection.NewDBConnection(conf)
	if errDBConn != nil {
		log.Fatalf("unable to connect to database, %v", errDBConn)
	}

	repo := registry.NewRepo(dbConn)
	ctx := context.Background()

	drop := database.NewDrop(dbConn)

	errDBReset := drop.Reset(ctx)
	if errDBReset != nil {
		log.Fatalf("unable to reset database, %v", errDBReset)
	}

	seed := seeder.NewSeeder(repo)
	if errSeed := seed.Seed(); errSeed != nil {
		log.Fatalf("unable to seed database, %v", errSeed)
	}

	hdl := handler.New(
		handler.WithConfig(conf),
		handler.WithRepo(repo),
	)

	return &TestSuite{
		Handler: hdl,
		Ctx:     ctx,
		Repo:    repo,
		Config:  conf,
	}
}

// IsTestMode is function to check if runtime running on test mode (go test)
func IsTestMode() bool {
	return flag.Lookup("test.v").Value.(flag.Getter).Get().(bool)
}
